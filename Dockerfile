FROM java:jdk

COPY . /usr/src/myapp
WORKDIR /usr/src/myapp

# Modify per your build tool
RUN ./gradlew clean build
CMD ["./gradlew", "run"]