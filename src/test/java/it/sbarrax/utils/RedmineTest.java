/**
 * 
 */
package it.sbarrax.utils;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.taskadapter.redmineapi.IssueManager;
import com.taskadapter.redmineapi.RedmineException;
import com.taskadapter.redmineapi.RedmineManager;
import com.taskadapter.redmineapi.RedmineManagerFactory;
import com.taskadapter.redmineapi.bean.Issue;
import com.taskadapter.redmineapi.bean.IssueFactory;

/**
 * @author sbarrax
 *
 */
public class RedmineTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		
		// TODO external variables
		String uri = "http://comredmine.zucchetti.com";
		String apiAccessKey = "26153729d713bf560154562f66b5cb97a96519c5";
		String projectKey = "fui";
		Integer queryId = null; // any

		RedmineManager mgr = RedmineManagerFactory.createWithApiKey(uri, apiAccessKey);
		IssueManager issueManager = mgr.getIssueManager();
		List<Issue> issues = null;
//		try {
//			issues = issueManager.getIssues(projectKey, queryId);
//			for (Issue issue : issues) {
//				System.out.println(issue.toString());
//			}
//		} catch (RedmineException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		// Create issue
		// Issue issueToCreate = IssueFactory.createWithSubject("some subject");
		// try {
		// Issue createdIssue = issueManager.createIssue(projectKey ,
		// issueToCreate);
		// } catch (RedmineException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		// Get issue by ID:
		try {
			Issue issue = issueManager.getIssueById(1832);
			System.out.println(issue.toString());
		} catch (RedmineException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
